import time, math, random
from multiprocessing import Process, Manager, Value, Array, Lock


class MultiObj:
    def __init__(self):
        self.manager = Manager()
        self.array = self.create_array()
        self.lock = Lock()
        self.locks = []
        for i in range(0, 10):
            self.locks.append(Lock())

    def __del__(self):
        self.manager.shutdown()

    def create_array(self):
        array_0 = self.manager.list()
        for x in range(0, 10):
            array_1 = self.manager.list()
            for y in range(0,10):
                array_2 = self.manager.list()
                for z in range(0,10):
                    array_2.append(0)
                array_1.append(array_2)
            array_0.append(array_1)

        return array_0

    def set_to(self, x, y, z, val):
        self.array[x][y][z] = val

    def add_to(self, x, y, z, val):
        self.array[x][y][z] = self.array[x][y][z] + val

    def multi_add_to(self, x, y, z, val):
        processes = []
        for i in range(3):
            processes.append(Process(target=self.add_worker_proc, args=(x,y,z,1,)))
        for p in processes:
            p.start()
        t = time.time()
        while len(processes) > 0:
            # print(self.array[x][y][z])
            for p in processes:
                if not p.is_alive():
                    p.join()
                    processes.remove(p)

    def add_worker_proc(self,x,y,z, val):
        for i in range(0, 100):
            # time.sleep( random.random() / 5)
            with self.locks[x]:
                self.array[x][y][z] = self.array[x][y][z] + val


    def get_val(self, x, y, z):
        return self.array[x][y][z]

    def do_something(self):
        pass


class App:
    def __init__(self):
        self.mul_ob = MultiObj()

    def run(self):
        print(f"### Single Thread Test")
        print(self.mul_ob.get_val(1,1,1))
        self.mul_ob.set_to(1,1,1,3.1316)
        print(self.mul_ob.get_val(1,1,1))
        self.mul_ob.add_to(1, 1, 1, 1)
        print(self.mul_ob.get_val(1, 1, 1))

        print(f"### Multi Process Test (Multi_add in Obj)")
        print(self.mul_ob.get_val(1,1,1))
        self.mul_ob.multi_add_to(1,1,1,1)
        print(self.mul_ob.get_val(1,1,1))

        print(f"### Multi Process Test (Multi_add in App")
        print(self.mul_ob.get_val(1,1,1))
        proc_1 = Process(target=self.worker_process, args=(1,))
        proc_1.start()
        print(self.mul_ob.get_val(1,1,1))
        proc_1.join()

        del(self.mul_ob)


    def worker_process(self, val):
        for i in range(10):
            arrray = self.mul_ob.array[1]
            time.sleep(0.1)
            self.mul_ob.add_to(1,1,1, val)



if __name__ == "__main__":
    app = App()
    app.run()