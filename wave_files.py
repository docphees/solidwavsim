import wave, struct

def import_wave(filename):
    wavefile = wave.open(filename, 'r')

    with wave.open(filename, 'r') as f:
        length = wavefile.getnframes()
        for i in range(0, length):
            wavedata = wavefile.readframes(1)
            data = struct.unpack("<h", wavedata)
            print(int(data[0]))


def export_wave(filename, data:list):
    sampleRate = 44100.0 # hertz
    with wave.open(filename, 'w') as f:
        f.setnchannels(1)
        f.setsampwidth(2)
        f.setframerate(sampleRate)
        # max amp: -32767 bis 32767
        for val in data:
            d = struct.pack('<h', val)
            f.writeframesraw(d)

if __name__ == "__main__":
    import math, array
    data = []
    freq = 440 # Hz
    for i in range(0, 120000):
        data.append(round(28000 * math.sin(6.29 / 44100 * (i * i/44100) * freq)))
    print(f"Data with {len(data)} entries generated.")
    export_wave("test.wav", data)