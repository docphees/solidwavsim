"""
##############################
   DOCPHEES' PYTHON LIBRARY
##############################

This library holds often reused code functions and classes.
The library is released under the MIT license and can freely be reused.

Version and changes:
0.1.0
- renamed _Getch class to Getch (keeping _Getch for compatibility until 0.2.0, deprecated!)
- added Killer class for simple SIGINT, SIGTERM catching
0.0.7
- added the iswin(), isosx() and islinux() functions.
- using is[os] functions in "clear"
0.0.6
- added clear, the ultimate clear screen function
0.0.5
- smaller _Getch class
0.0.4
initial release containing
- class _Getch
- class Version
- text cleaning
	removeHtmlTags()
	removeLeadingSpaces()
	cleanText()
- function printhist()
- function printtitle()

Version history:
[none]
"""
version = [0,0,7]
modulename = "pydpyl"

##### VERSION STRING HANDLING
class Version:
    """ The Version object stores and handles simple version numbering.

    object = pydpyl.Version(0,1,2)      sets version, subversion and minor version
    object([depth])                     returns the version as string
                                        (depth= 1: version, 2: ver+sub, 3: ver+sub+minor)
    object(num=True)                    returns the version as [ver, sub, minor], depth is ignored
    """
    def __init__(self, ver=0, sub=0, min=0):
        self.version = [int(ver), int(sub), int(min)]   # version, subversion and minor version

    def __call__(self, depth=3, num=False):   #calling the object returns the version as string
        versionstring = str(self.version[0])
        if not num:
            for n in range(1, depth):
                versionstring += "."+str(self.version[n])
            return versionstring
        else:
            return self.version

#######################################################################
##### A non-blocking get-a-single-character-or-nothing from standard input
##### The _Getch class
#####
##### usage: import pydpyl
##### getch = pydpyl._Getch(5) # the 5 is a 5 second timeout. you can use a 0 second timeout in a loop!
##### input = getch()
##### input now contains one character or nothing ("")

class Getch:
    """Gets a single character from standard input. Does not echo to the screen."""
    def __init__(self, to = 1):
        self.timeout = to
        try:
            self.impl = _GetchWindows()
        except ImportError:
            self.impl = _GetchUnix(to)

    def __call__(self):
        return self.impl()

class _Getch(Getch):
	""" Deprecated!! Kept for compatibilitie's sake!"""
	def __init__(self, to = 1):
		print("The pydpyl '_Getch' is deprecated. Use 'Getch' class instead!")
		super(self.__class__, self).__init__(to)

class _GetchUnix:
    def __init__(self, to):
        self.timeout = to
        import tty
        self.tty = tty
        import sys
        self.sys = sys
        import termios
        self.termios = termios
        from select import select
        self.select = select

    def __call__(self):
        #import termios
        # from select import select
        fd = self.sys.stdin.fileno()
        old_settings = self.termios.tcgetattr(fd)
        try:
            self.tty.setraw(self.sys.stdin.fileno())
            [i, o, e] = self.select([self.sys.stdin.fileno()], [], [], self.timeout)
            if i:
                ch=self.sys.stdin.read(1)
            else:
                ch=''
        finally:
            self.termios.tcsetattr(fd, self.termios.TCSADRAIN, old_settings)
        return ch

class _GetchWindows:
    def __init__(self):
        import msvcrt

    def __call__(self):
        return msvcrt.getch()


###################
##### Graceful Killer

class Killer:
    def __init__(self):
        self.killed = False
        import signal
        signal.signal(signal.SIGINT, self._exit_gracefully)
        signal.signal(signal.SIGTERM, self._exit_gracefully)

    def _exit_gracefully(self, signum, frame):
        self.killed = True


#################################################
#################################################
##### FUNCTIONS
#################################################
#################################################

###################
##### Is[OS]
# These functions return a boolean True if the os matches.

def iswin():
    import os
    if os.name in ('ce', 'nt', 'dos'):
        return True
    else:
        return False
def isosx():
    import os
    if os.name in ('osx'):
        return True
    else:
        return False
def islinux():
    import os
    if os.name in ('posix', 'linux'):
        return True
    else:
        return False


###################
##### Clear
# The clear function works on any OS and chooses the appropriate method for
# a clear screen
def clear():
    import os
    #if os.name in ('ce','nt','dos'):
    if iswin():
        os.system('cls')
        return 'clear'     #returns the method used
    #elif os.name in ('posix','osx','linux'):
    elif islinux() or isosx():
        try:
            os.system('clear')
            return 'clear'  # returns the method used
        except:
            print('\n' * 120)
            return 'lines'     #returns the method used
    else:
        print('\n'*120)
        return 'lines'     #returns the method used
    return


###################
##### Clean Strings
# These functions should be used with one-line strings
# If necessary use str.splitlines() and push the lines through the cleanText functions.
# This is very basic and so far does not provide any security at all
def removeHtmlTags(text):
    import re
    text = str(text)
    tag_re = re.compile(r'<[^>]+>')
    return tag_re.sub('', text)

def removeLeadingSpaces(text):
    text = str(text)
    for n in range(len(text)):
        if text[n] != " ":
            break
    return text[n:]

def cleanText(text):
    text = str(text)
    return removeLeadingSpaces(removeHtmlTags(text))

################
##### PrintTitle
def printtitle(title=""):
    if str(title) != "":
        print(" .-" + len(str(title)) * "-" + "-.")
        print(" | " + str(title) + " |")
        print(" '-" + len(str(title)) * "-" + "-'")
    return 0

#################
##### PrintHist()
def printhist(dataset, height=10, lowest="", highest="", columnw=3, title=""):
    """ Printhist prints a simple diagram/histogram of the
    values in a list, list of lbl/value pairs or dictionary.

    usage: printhist(dataset, height, min, max, columnwidth, title)

    dataset     any list of numbers
                or a dictionary
                or a list of lbl/data pairs like [ [1,5], ["hi",4] ]
    height      lines of the histogram (+1)
    lowest      manual selection of lowest value, must be integer!
    highest     manual selection of highest value, must be integer!
    columnw     width of each data point's column

    if lowest and highest are not set they are determined automatically.
    """


    # generate two lists, lbl and data, for label and data (number)
    lbl = []
    data = []


    if isinstance(dataset, list):
        # sanitise the dataset a bit and generate a label list, starting with 1 (!)
        if isinstance(dataset[0],list):
            for n in range(len(dataset)):
                lbl.append(str(dataset[n][0]))
                data.append(dataset[n][1])
        else:
            for n in range(len(dataset)):
                lbl.append(str(n+1))
                if type(dataset[n]) in [float, int]:
                    data.append(dataset[n])
                else:
                    data.append(0)
    elif isinstance(dataset, dict):
        # split the dataset into a label list and a data list
        for n in dataset:
            lbl.append(str(n))
            if type(dataset[n]) in [float, int]:
                data.append(dataset[n])
            else:
                data.append(0)

    if lowest == "":
        lowest = min(data)
    if highest == "":
        highest = max(data)
    step = ((highest - lowest) / (height))

    # print a title for the graph
    printtitle(title)

    # create formatstring in style of "{:=7.2f}" for y-value formatting
    formatstring = "{:=" + str(max(len(str(int(highest))), len(str(int(lowest)))) + 4) + ".2f}"
    for n in range(height, -1, -1):
        linestr = ""
        for i in range(len(data)):
            if data[i] >= n * step + lowest and data[i] < (n + 1) * step + lowest:
                # linestr += "*  "
                if i > 0:
                    if data[i - 1] < n * step + lowest:
                        linestr += ","
                    elif data[i - 1] >= (n + 1) * step + lowest:
                        linestr += "`"
                    else:
                        linestr += "-"
                else:
                    linestr += " "
                # if columnwidth > 2 then fill up with "-" characters
                linestr += (columnw - 2) * "-"
                # if columnwidth > 1, create a right border character for the graph
                if columnw > 1:
                    if i < len(data) - 1 and columnw > 1:
                        if data[i + 1] < n * step + lowest:
                            linestr += "."
                        elif data[i + 1] >= (n + 1) * step + lowest:
                            linestr += "'"
                        else:
                            linestr += "-"
                    else:
                        linestr += " "
            else:
                if i > 0:
                    if (data[i - 1] >= (n + 1) * step + lowest and data[i] < n * step + lowest) or (
                            data[i - 1] < n * step + lowest and data[i] >= (n + 1) * step + lowest):
                        linestr += "|"
                    else:
                        linestr += " "
                else:
                    linestr += " "
                linestr += (columnw - 1) * " "

        #now print the line
        print(formatstring.format((n * step + lowest)) + ": " + linestr)

    linestr = ""

    # find longest label(text)
    lbl_maxlen=0
    for n in range(len(lbl)):
        lbl_maxlen = max(lbl_maxlen, len(str(lbl[n])))

    # create linestrings and print the label-lines
    for n in range(lbl_maxlen):
        linestr = (max(len(str(int(highest))), len(str(int(lowest)))) + 4 + 3) * " "
        for i in range(len(lbl)):
            if len(lbl[i]) > n:
                linestr += lbl[i][n]
            else:
                linestr += " "
            linestr += (columnw - 1) * " "
        print(linestr)


def printhist_testsuite():
    import math
    testname="test 1"
    print("=====", testname, "=====")
    tmp = [2, 3, 1, -1, 0, 5, 6, 5, 3, 6.9]
    printhist(tmp, 10)
    printhist(tmp, 5, columnw=2)

    testname="test 2"
    tmp = {}
    print("=====", testname, "=====")
    tmp = {0: 99892, 1: 100096, 2: 100086, 3: 100317, 4: 100044, 5: 99697, 6: 99962, 7: 99868, 8: 100064, 9: 99974, 10: 10092, 11: 8988, 12: 9767, 13: 10037, 14: 9967, 15: 10062, 16: 9975, 17: 10070, 18: 10135, 19: 10104}
    printhist(tmp, 10)
    printhist(tmp, 20, 0, columnw=3)

    testname = "test 3"
    print("=====",testname, "=====")
    tmp = []
    z = 2 * math.pi
    while z < 6 * math.pi:
        tmp.append([ '{:4.1f}'.format(z), math.sin(z)])
        z += ( (6 * math.pi - 2 * math.pi) / 24)

    printhist(tmp, 10, columnw = 2, title="sinus, 2 pi --> 4 pi")

    tmp = []
    z = 2 * math.pi
    while z < 6 * math.pi:
        tmp.append([ '{:4.1f}'.format(z), math.sin(z)])
        z += ( (6 * math.pi - 2 * math.pi) / 64)
    printhist(tmp, 8, columnw=1, title="sinus, 2 pi --> 4 pi")


######################################################
##### simpleGetHtml
#####
##### gets the html-text from a valid url using urllib

def simpleGetHtml(url=""):
    import urllib.request
    url = str(url)
    html = ""
    if url != "":
        opener = urllib.request.FancyURLopener({})
        f = opener.open(url)
        html = f.read()
        return html
    else:
        return -1


###############################################
###############################################
# Running pydpyl
###############################################
###############################################
if __name__ == "__main__":
    ver = Version(version[0], version[1], version[2])
    print("pydpyl, version", ver(3))

    getch = _Getch(5)
    print("0 for all testsuites")
    print("q to quit")
    input = getch()
    if input == "0":
        printhist_testsuite()
        print("Weiter")
        input = getch()
    elif input in [ "q", ""]:
        quit()
