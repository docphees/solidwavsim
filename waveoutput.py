#!python3
import wave, struct


VERSION, SUBVERSION = "1","01"
class BoundedNumber:
    def __init__(self, val:float, min_n, max_n):
        print([min_n, max_n])
        (self._min, self._max) = sorted((min_n, max_n))
        self.set(val)

    def set(self, val):
        self.val = min(max(self._min, val), self._max)

    def __call__(self):
        return self.val


class WaveOutputNew:
    # TODO: Put this into another process, connected via pipe
    #       close file_output if connection drops / pipe disconnects / timeout
    #       Receives chunks of data, writes them to the file_output
    #       pauses for a moment, looks at pipe, checks timeout, closes
    # TODO: Maybe just write the logged data to a tmp / text file_output and convert into
    #       a wave file_output when the simulation has ended
    # TODO: Make this ONLY ONE filehandler per stream OR multiple / n streams, NOT just two (pri/sec)

    class _stream:
        def __init__(self, filename, smplrate, smplwidth, channels=1, capture_input=False, chunk_size=100):
            self.filepath = "./"
            self.filename = str(filename)
            self.smplrate = int(smplrate)
            self.smplwidth = int(smplwidth)
            self.max_val = int((254 ** smplwidth) / 2)
            self.channels = int(channels)
            self.capture_input = bool(capture_input)
            self.data_output = []
            self.data_input = []
            self.chunk_size = int(chunk_size)
            self.file_extension = ".wav"
            self.file_extension_tmp = ".tmp"
            self.fullpath_output = ""
            self.fullpath_input = ""
            self.fullpath_output_tmp = ""
            self.fullpath_input_tmp = ""
            self.file_output = None  # TODO: deprec. ?
            self.file_input = None  # TODO: deprec. ?

            self.fullpath_output = self.filepath + self.filename + "_pri" + self.file_extension
            self.fullpath_input = self.filepath + self.filename + "_sec" + self.file_extension
            self.fullpath_output_tmp = self.filepath + self.filename + "_pri" + self.file_extension_tmp
            self.fullpath_input_tmp = self.filepath + self.filename + "_sec" + self.file_extension_tmp
    def __init__(self):
        self.steams = {}
        pass

    def close(self):
        pass

    def add_stream(self, filename, smplrate, smplwidth, channels=1, capture_input=False, chunk_size=100):
        self.filepath = "./"
        self.filename = str(filename)
        self.smplrate = int(smplrate)
        self.smplwidth = int(smplwidth)
        self.max_val = int((254 ** smplwidth) / 2)
        self.channels = int(channels)
        self.capture_input = bool(capture_input)
        self.data_output = []
        self.data_input = []
        self.chunk_size = int(chunk_size)
        self.file_extension = ".wav"
        self.file_extension_tmp = ".tmp"
        self.fullpath_output = ""
        self.fullpath_input = ""
        self.fullpath_output_tmp = ""
        self.fullpath_input_tmp = ""
        self.file_output = None     # TODO: deprec. ?
        self.file_input = None      # TODO: deprec. ?

        stream = self._Stream(filename, smplrate, smplwidth, channels, capture_input, chunk_size)

        self.fullpath_output = self.filepath + self.filename + "_pri" + self.file_extension
        self.fullpath_input = self.filepath + self.filename + "_sec" + self.file_extension
        self.fullpath_output_tmp = self.filepath + self.filename + "_pri" + self.file_extension_tmp
        self.fullpath_input_tmp = self.filepath + self.filename + "_sec" + self.file_extension_tmp

    def add_stream(self):
        # craeate a setup per stream (dict config?) and let the handler run in a separate process
        # every stream should accept data and can be closed seperately
        # connect the handler via a pipe, let it distribute the logged data internally
        pass

    def open(self):
        # create empty temp-files
        open(self.fullpath_output_tmp, "w").close()
        if self.capture_input:
            open(self.fullpath_input_tmp, "w").close()

    def write(self, value_output, value_input=None):
        # Appends value to the two data chunks and triggers writing to file if chunks are full
        self.data_output.append(value_output)
        if self.capture_input:
            self.data_input.append(value_input)

        if len(self.data_output) >= self.chunk_size:
            self.write_to_file()

    def write_to_file(self):
        # empties data chunks into temp files
        # write process output data to temp file
        with open(self.fullpath_output_tmp, "a") as f_out:
            for val in self.data_output:
                f_out.writelines(str(val) + "\n")
            self.data_output.clear()

        if self.capture_input:
            # write original input_data to temp file
            with open(self.fullpath_input_tmp, "a") as f_in:
                for val in self.data_input:
                    f_in.writelines(str(val) + "\n")
                self.data_input.clear()

    def analyze_temp_file(self, filepath):
        maxN = 0
        # do the analysis
        # 1. Find maximum value in the file
        with open(filepath, "r") as f:
            for line in f:
                maxN = max(maxN, float(line))

        # if all values were 0, return a max of 1 to avoid division by 0
        if maxN == 0:
            maxN = 1
        # calculate normalization factor
        factor_norm = (self.max_val - 1) / maxN
        return factor_norm

    def convert_temp_file(self, file_in, file_out, factor):
        # TODO: normalization optional? If disabled, the wave files might clip.
        # BoundedNumber clamps a .set(val) value to the max and min
        bound_n = BoundedNumber(0, -self.max_val, self.max_val)

        with open(file_in, "r") as f_in:
            with wave.open(file_out, "w") as f_out:
                f_out.setnchannels(self.channels)
                f_out.setsampwidth(self.smplwidth)
                f_out.setframerate(self.smplrate)
                for line in f_in:
                    # normalize the data with the factor from .analyze_temp_file()
                    bound_n.set(int(float(line) * factor))
                    val_struct = struct.pack('<h', bound_n())
                    f_out.writeframesraw(val_struct)

    def close(self, convert_to_wave=True, normalize=True):
        # empty data chunks
        self.write_to_file()
        # get scaling factor from temp files and convert to wave file
        factor_output = self.analyze_temp_file(self.fullpath_output_tmp)
        self.convert_temp_file(self.fullpath_output_tmp, self.fullpath_output, factor_output)
        # TODO: delete temp file

        if self.capture_input:
            factor_input = self.analyze_temp_file(self.fullpath_input_tmp)
            self.convert_temp_file(self.fullpath_input_tmp, self.fullpath_input, factor_input)
            # TODO: delete temp file


class WaveOutput:
    # TODO: Put this into another process, connected via pipe
    #       close file_output if connection drops / pipe disconnects / timeout
    #       Receives chunks of data, writes them to the file_output
    #       pauses for a moment, looks at pipe, checks timeout, closes
    # TODO: Maybe just write the logged data to a tmp / text file_output and convert into
    #       a wave file_output when the simulation has ended
    # TODO: Make this ONLY ONE filehandler per stream OR multiple / n streams, NOT just two (pri/sec)
    def __init__(self, filename, smplrate, smplwidth, channels=1, capture_input=False, chunk_size=100):
        self.filepath = "./"
        self.filename = str(filename)
        self.smplrate = int(smplrate)
        self.smplwidth = int(smplwidth)
        self.max_val = int((254 ** smplwidth) / 2)
        self.channels = int(channels)
        self.capture_input = bool(capture_input)
        self.data_output = []
        self.data_input = []
        self.chunk_size = int(chunk_size)
        self.file_extension = ".wav"
        self.file_extension_tmp = ".tmp"
        self.fullpath_output = ""
        self.fullpath_input = ""
        self.fullpath_output_tmp = ""
        self.fullpath_input_tmp = ""
        self.file_output = None     # TODO: deprec. ?
        self.file_input = None      # TODO: deprec. ?

        self.fullpath_output = self.filepath + self.filename + "_pri" + self.file_extension
        self.fullpath_input = self.filepath + self.filename + "_sec" + self.file_extension
        self.fullpath_output_tmp = self.filepath + self.filename + "_pri" + self.file_extension_tmp
        self.fullpath_input_tmp = self.filepath + self.filename + "_sec" + self.file_extension_tmp

    def add_stream(self):
        # craeate a setup per stream (dict config?) and let the handler run in a separate process
        # every stream should accept data and can be closed seperately
        # connect the handler via a pipe, let it distribute the logged data internally
        pass

    def open(self):
        # create empty temp-files
        open(self.fullpath_output_tmp, "w").close()
        if self.capture_input:
            open(self.fullpath_input_tmp, "w").close()

    def write(self, value_output, value_input=None):
        # Appends value to the two data chunks and triggers writing to file if chunks are full
        self.data_output.append(value_output)
        if self.capture_input:
            self.data_input.append(value_input)

        if len(self.data_output) >= self.chunk_size:
            self.write_to_file()

    def write_to_file(self):
        # empties data chunks into temp files
        # write process output data to temp file
        with open(self.fullpath_output_tmp, "a") as f_out:
            for val in self.data_output:
                f_out.writelines(str(val) + "\n")
            self.data_output.clear()

        if self.capture_input:
            # write original input_data to temp file
            with open(self.fullpath_input_tmp, "a") as f_in:
                for val in self.data_input:
                    f_in.writelines(str(val) + "\n")
                self.data_input.clear()

    def analyze_temp_file(self, filepath):
        maxN = 0
        # do the analysis
        # 1. Find maximum value in the file
        with open(filepath, "r") as f:
            for line in f:
                maxN = max(maxN, float(line))

        # if all values were 0, return a max of 1 to avoid division by 0
        if maxN == 0:
            maxN = 1
        # calculate normalization factor
        factor_norm = (self.max_val - 1) / maxN
        return factor_norm

    def convert_temp_file(self, file_in, file_out, factor):
        # TODO: normalization optional? If disabled, the wave files might clip.
        # BoundedNumber clamps a .set(val) value to the max and min
        bound_n = BoundedNumber(0, -self.max_val, self.max_val)

        with open(file_in, "r") as f_in:
            with wave.open(file_out, "w") as f_out:
                f_out.setnchannels(self.channels)
                f_out.setsampwidth(self.smplwidth)
                f_out.setframerate(self.smplrate)
                for line in f_in:
                    # normalize the data with the factor from .analyze_temp_file()
                    bound_n.set(int(float(line) * factor))
                    val_struct = struct.pack('<h', bound_n())
                    f_out.writeframesraw(val_struct)

    def close(self, convert_to_wave=True, normalize=True):
        # empty data chunks
        self.write_to_file()
        # get scaling factor from temp files and convert to wave file
        factor_output = self.analyze_temp_file(self.fullpath_output_tmp)
        self.convert_temp_file(self.fullpath_output_tmp, self.fullpath_output, factor_output)
        # TODO: delete temp file

        if self.capture_input:
            factor_input = self.analyze_temp_file(self.fullpath_input_tmp)
            self.convert_temp_file(self.fullpath_input_tmp, self.fullpath_input, factor_input)
            # TODO: delete temp file



if __name__ == "__main__":
    print("waveoutput.py")
    print(f"============= v{VERSION}.{SUBVERSION}")
    print("This python3 file provides the following classes:")
    print("")
    print("- WaveOutput")
    print("A simplified class for wave file generation (audio) from large datasets")
    print("")
    print("- BoundedNumber")
    print("A class for numbers that are kept within min/max bounds")
    print("")
