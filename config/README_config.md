# SOLIDWAVSIM Config File
This is how a config file should work.
Currently only the input file and output file sections
are implemented, together with inputtype and outputtype.

[DEFAULT]
size_x = 30      # Physical dimensions of the space
size_y = 30      # to be simulated in number of element blocks
size_z = 3       # (has to be filled by objects/material)
length = 1       # cm, length of one element block
objects = air, slab1, slab2, air2   # unique names
    # fills up the simulation space. later overwrites!
    # names have to be unique(!) and will be defined 
    # below, using an "object_" prefix
input = sine   # sine, file
output = screen   # screen (only), file (also)
refresh = 100    # refresh graph every x steps

[input_sine]      # if inputtype is sine
freq = 440   # in Hz
amplitude = 0.3  # in units
duration = 1     # in seconds, -1 for infinity
dt = 0.00001   # simulation step size in seconds

[input_file]    # if inputtype is file
filename = sine_440hz.wav    # filename of a wave file
channels = 1      # channel number of the wave file
smplrate = 22050  # Hz, sampling rate of the wave file
smplwidth =  2 ??     # bitrate / resolution ???

[output_file]   # if outputtype is file
filename = sine_440hz_output.wav  # filename of the wave output file
smplrate = 1      # sampling rate of the simulation
smplwidth =  2 ??   # bitrate / resolution

[object_air]    # an object as defined in [DEFAULT][objects]
type = slab      # geometries = slab (sphere, circle NOT IMPLEMENTED)
x = (0,10)  # ranges
y = (0,5)   # ranges
z = (2,3)   # ranges
hardness =   # better names
friction =   # better names

[object_slab1]   # another object
(...)

