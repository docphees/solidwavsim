
# import numpy
import time, math
import configparser
import wave, struct
import signal
import random
from threading import Thread, Event, Lock
from multiprocessing import Process, Manager, Value, Array, Pipe
import pydpyl
from waveoutput import WaveOutput
import signalgenerator as SG

LOG_VERBOSITY = 4   # 0-10, 0 being silent, 5 important structures like lists, 9 EVERYTHING
LOG_TARGET = "terminal"   # log to either "terminal" or "file_out"
LOG_FILE = None
MULTITHREAD = False
MULTIPROCESS = False

QUIT = False

# SIMPLE LOGGING TOOL
def log(verb, content):
    """
    Logs either to the terminal or a file_pri if verb is lower or equal to the verbosity level.
    logging is controlled by these global variables:

    LOG_VERBOSITY - 0-10, 0 being completely silent, 5 should be certain critical constructs like
        important arrays, 10 should include everything. There is no limit to LOG_VERBOSITY, feel
        free to define your 2001 verbosity levels.
    LOG_TARGET - "terminal" or "file_pri"
    LOG_FILE - None or a valid file_pri path. The file_pri will be created if not present. The full path
         will not be created.

    :param verb: int
    :param content: printable object
    :return:
    """
    if verb <= LOG_VERBOSITY:
        if LOG_TARGET == "terminal":
            print(content)
        elif LOG_TARGET == "file_pri":
            if LOG_FILE is not None:
                pass
                # check for file_pri, create file_pri, log to file_pri etc.

def v_add(a:tuple, b:tuple, tuple = False):
    result = [0.0,0.0,0.0]
    for i in range(len(a)):
        result[i] = a[i] + b[i]
    if tuple:
        result = tuple(result)
    return result

def v_add_to(a:list, b):
    for i in range(len(a)):
        a[i] += b[i]

def v_scal_mult(a:tuple, b:float):
    result = []
    for i in a:
        result.append(a * b)
    return result

class Timy:

    def __init__(self):
        # Data is stored as dicts in dict: { id:{title:str, data_start:float, data_stop:float}, ... }
        self.data = {}
        self.data_title = None
        self.data_start = None
        self.data_stop = None
        self.history:[{}] = []

    def start(self, data_id):
        data_id = str(data_id)
        if id not in self.data:
            if len(self.data) < 10000:
                self.data[data_id] = {"title":None, "data_start":time.time(), "data_stop":None, "result":None}
            else:
                print("Timey: Too many (>9.999) simultaneous Timey measurements")
                print("       stop some measurements or clear out the stack (Timey.clear()).")

    def stop(self, data_id, history=False, title=None):
        value = -1
        data_id = str(data_id)
        if data_id in self.data:
            dataset = self.data[data_id]
            dataset["data_stop"] = time.time()
            dataset["title"] = title

            dataset["result"] = value = dataset["data_stop"] - dataset["data_start"]

            if history:
                self.history.append(dataset)
            # remove data entry from measurement dictionary
            self.data.pop(data_id)
        else:
            log(1, f"Timer with id '{data_id}' cannot be stopped: It has not been started!")

        return value

    def get_history(self, index=-1):
        """
        Returns a history entry like this: {"title":str, "time":time}.
        The index parameter can be positive (up) or negative (down).
        The index is max/min limited to the first/last entry.
        get_history(int) will always return a result. If none is present, title is None, time is 0.
        par: index - integer
        """
        if len(self.history) == 0:
            return {"title":"NONE", "time":0}
        else:
            if index < 0:
                return self.history[max(-len(self.history), index)]
            if index >= 0:
                return self.history[min(len(self.history), index)]

    def clear_history(self):
        self.history = []

    def clear(self):
        self.data = {}


class Cell:
    def __init__(self, index, position, hardness=(0,0,5000000)):
        self.debug = False
        self.mass:float = 1.0
        self.position_rest = position   #(x,y,z) position. NOT the same as the index in the finite element matrix!
        self.displacement_last = [0.0, 0.0, 0.0]
        self.displacement = [0.0, 0.0, 0.0]
        self.displacement_next = [0.0, 0.0, 0.0]
        self.force = [0.0, 0.0, 0.0]
        self.force_last = [0.0, 0.0, 0.0]
        self.velocity = [0.0, 0.0, 0.0]
        self.spring_hardness = hardness   # hardness in each dimensional direction # TODO: Hardness is crap!
        self.dampening = (0.0, 0.0, 0.8)    # dampening in each dimensional direction # TODO: Dampening is crap!
        self.index = index    # (x,y,z) 'coordinates' in the finite element matrix
        self.neighbours = []     # a list of cells

        if self.position_rest is None:
            self.position_rest = self.index

    def get_com(self):
        return self.index

    def set_debug(self, flag=True):
        self.debug = flag

    def force_calculate(self):
        # TODO: For now *only* z-forces are calculated
        # TODO: Neighbour Reduction Part 2 (Cell): Two-Way calculations to reduce calculations
        force_finite_z = 0
        force_friction_z = 0
        force_relocation_z = 0
        for nb in self.neighbours:
            # FORCES BETWEEN FINITE ELEMENTS which is pointing from self to the neighbour
            force_finite_z = (nb.displacement[2] - self.displacement[2]) * (nb.spring_hardness[2] + self.spring_hardness[2]) / 2

            # FRICTION FORCES (results in dampening)
            # relative velocity with neighbour as seen from cell
            v_self = self.velocity[2]
            v_rel = nb.velocity[2] - v_self
            force_friction_z = 100 * math.copysign(v_rel ** 2, v_rel)  # transfers sign to first value
            # TODO: .5 is an arbitrary value. Get physical values!
            # global velocity to dampen oscillations of the body
            damping_global = .5
            # force_friction_z += 1 * math.copysign(v_self ** 2, -v_self)  # transfers sign to first value

            # RELOCATING FORCE (prohibiting the slab from wandering around)
            # TODO: This should really only work on the corners / fixation points!
            # force_relocation_z = - 1 * self.displacement[2]

            # if self.debug: print(f"finite: {force_finite_z} friction: {force_friction_z} relocation: {force_relocation_z}")
            self.force_receive((0,0,force_finite_z+force_friction_z+force_relocation_z))
            nb.force_receive((0,0,-force_finite_z-force_friction_z))

        return (0,0,force_finite_z)


    def force_receive(self, force:(float,float,float)):
        """
        Receive a force from a neighbour or otherwise.
        Force is given as a three-dimensional tuple
        :param force: (float,float,float)
        :return:
        """
        v_add_to(self.force, force)

    def set_force(self, force:(float,float,float)):
        self.force = [force[0], force[1], force[2]]

    def set_excitation(self, dislocation, dt):
        # TODO: deprec)
        for i in range(3):
            # self.velocity[i] = (dislocation[i] - self.displacement[i]) / dt
            self.velocity[i] = 0
        self.displacement = list(dislocation)

    def set_velocity(self, velocity):
        for i in range(3):
            self.velocity[i] = velocity[i]

    def get_index(self):
        return self.index

    def get_position(self):
        return v_add(self.position_rest, self.displacement)

    def tick(self, dt):
        # --------NEW---------
        # 1. distribute forces / receive forces
        # 2. let forces act (change velocities and clear out forces)

        # calculate new velocity
        velocity_new = [0,0,0]
        for i in range(3):
            velocity_new[i] = self.velocity[i] + dt * self.force[i] / self.mass
        # Alternative: velocity_new = v_add(self.velocity, v_scal_mult(self.force, dt/self.mass))

        # calculate new displacement
        for i in range(3):
            self.displacement[i] += dt * (self.velocity[i] + velocity_new[i]) / 2
        # Alternative: self.displacement = v_add(self.displacement, v_scal_mult(v_add(self.velocity, velocity_new), dt/2))

        self.velocity = velocity_new
        self.force = [0,0,0]

        return self.displacement[2], self.velocity[2]

    def __str__(self):
        line = ""
        line += f"pos: {self.index} hard: {self.spring_hardness} neighbours: {len(self.neighbours)}"
        return line


class Body:
    """Defines a Hard-body, consisting of Voxel-Cells"""
    def __init__(self, element_length, slab:dict=None, base:tuple=(0,0,0)):
        """
        Create a Body class object.
        Use one of the geometry classes:
            - slab: describing the dimensions of a slab class object
        :param slab: optional: (x,y,z) three dimensional tuple, default: None
        :param base: optional: (x,y,z) location of the fulcrum element, default: (0,0,0)
        """
        self.base = base
        self.size = (0, 0, 0)   # in cm
        self.element_length = element_length    # in cm

        self.worker_threads = []    # for parallel threads
        self.worker_processes = []    # for parallel processes


        if slab is not None:
            size = slab["size"]
            hardness = slab["hardness"]
            base = slab["base"]
            self.cells:[[[Cell]]] = self._create_slab_geometry(slab, self.element_length, hardness)


    def _create_slab_geometry(self, slab:dict, element_length:float, location:tuple=None):
        (s_x, s_y, s_z) = slab["size"]
        hardness = slab["hardness"]

        # TODO: Rename 'hardness' to 'schubmodul' or english
        # TODO: location not implemented

        #cells = [[[0]*s_z]*s_y]*s_x

        cells = [[[0 for _ in range(s_z)] for _ in range(s_y)] for _ in range(s_x)]

        hardness = (0,0, hardness[2] / element_length)

        # log(5, cells)
        for x in range(0, s_x):
            for y in range(0, s_y):
                for z in range(0, s_z):
                    print(f"Cell {x},{y},{z}")
                    cells[x][y][z] = Cell(index=(x, y, z), position=(x,y,z), hardness=hardness)
                    # log(5, cells[x][y][z])

        # log(5, cells); time.sleep(1)
        # set neighbours
        # log(5, f"x:{len(cells)} y:{len(cells[0])} z:{len(cells[0][0])}")
        n=0
        for x in range(0, len(cells)):
            for x in range(0, len(cells[0])):
                for z in range(0, len(cells[0][0])):
                    n += 1
                    # log(5, f"{n} cells({x},{y},{z}): {cells[x][y][z]}")

        self.size = (len(cells), len(cells[0]), len(cells[0][0]))

        # Create neighbour list per cell
        for x in range(s_x):
            for y in range(s_y):
                for z in range(s_z):
                    # for dislocation in ( (0,0,-1), (0,0,1), (0,-1,0), (0,1,0), (-1,0,0), (1,0,0)):
                    for dislocation in ((0, 0, 1), (0, 1, 0), (1, 0, 0)):
                        # TODO: Neighbour Reduction Part 1 (Body): Two-Way calculations to reduce calculations
                    #for dislocation in ((0, 0, -1), (0, 0, 1), (0, -1, 0), (0, 1, 0), (-1, 0, 0), (1, 0, 0), (0, 1, 1), (0, 1, -1), (0, -1, 1), (0, -1, -1)):
                        (dx,dy,dz) = dislocation
                        (nx, ny, nz) = (x+dx,y+dy,z+dz)
                        if nx>=0 and ny>=0 and nz>=0 and nx<len(cells) and ny<len(cells[0]) and nz<len(cells[0][0]):
                            pot_neighbour = cells[nx][ny][nz]
                            if pot_neighbour.__class__ == Cell:
                                cells[x][y][z].neighbours.append(pot_neighbour)
                                log(5, f"Neighbour added: {x+dx},{y+dy},{z+dz}")
                            else:
                                pass
                                log(5, f"Neighbour {x+dx},{y+dy},{z+dz} does not exist.")

        return cells[:]

    def excite(self, index:tuple=None, dislocation=(0,0,0), dt=None):
        if index is None:
            location = self.base
        (x,y,z) = index
        cell:Cell = self.cells[x][y][z]
        cell.set_excitation(dislocation, dt)

    def excite_velocity(self, index:tuple=None, velocity=(0,0,0), dt=None):
        if index is None:
            index = self.base
        (x,y,z) = index
        cell = self.cells[x][y][z]

        for i in range(3):
            if velocity.__class__ in (tuple, list):
                cell.velocity[i] = velocity[i]

    def excite_cell(self, index:tuple=None, velocity:tuple=None, displacement:tuple=None):
        if index is None:
            index = self.base
        (x,y,z) = index
        cell = self.cells[x][y][z]
        for i in range(3):
            if velocity.__class__ in (tuple, list):
                cell.velocity[i] = velocity[i]
            if displacement.__class__ in (tuple, list):
                cell.displacement[i] = displacement[i]


    def tick(self, dt):
        # distribute energies
        (s_x, s_y, s_z) = self.size
        if MULTITHREAD:
            self.tick_force_calculations_parallel_thread(dt, cuts=(1,0,0), autocut=True)
        elif MULTIPROCESS:
            self.tick_force_calculations_parallel_process(dt, cuts=(1,0,0), autocut=True)
        else:
            # one thread
            self.tick_force_calculations(dt)
            # another thread

        sum_pos = 0
        sum_vel = 0
        # tick the cells
        for x in range(0, s_x):
            for y in range(0, s_y):
                for z in range(0, s_z):
                    pos, vel =  self.cells[x][y][z].tick(dt)
                    sum_pos += pos
                    sum_vel += vel
        return sum_pos, sum_vel

    def tick_force_calculations(self, dt):
        (s_x, s_y, s_z) = self.size
        for x in range(0, s_x):
            for y in range(0, s_y):
                for z in range(0, s_z):
                    cell:Cell = self.cells[x][y][z]
                    if cell.__class__ == Cell:
                        cell.force_calculate()

    def tick_force_calculations_parallel_thread(self, dt, cuts:(int,int,int)=(1,0,0), autocut:bool=True):
        """
        Parallelizes force calculations. If autocut is True (default), it will try to
        determin the number of available cores and cut the calculations into chunks if
        a gain in computing time is considered possible.

        The number of cuts in each dimension can be set manually by using the cuts parameter. It
        must be a tuple of three integers, each number indicating the number of cuts in each dimension
        of the finite elements array.

        dt is the time step length of the calculation.

        TODO: Currently cuts and autocut don't do anything!

        :param dt: int
        :param cuts: tuple of three integers
        :param autocut: boolean flag
        :return:
        """
        (s_x, s_y, s_z) = self.size

        # TODO: Create Worker Threads ONCE and keep them, waiting for an event. (Find out how: multithreading.Event)

        # TODO: dim and cuts don't do anything yet to parallelize the calculations
        if len(self.worker_threads) == 0:
            lims = []
            lims.append( ((0, round(s_x / 2)), (0, round(s_y / 2)), (0, s_z)) )
            lims.append( ((round(s_x / 2), s_x), (0, round(s_y / 2)), (0, s_z)) )
            lims.append( ((0, round(s_x / 2)), (round(s_y / 2), s_y), (0, s_z)) )
            lims.append( ((round(s_x / 2), s_x), (round(s_y / 2), s_y), (0, s_z)) )

            # create and start threads
            for count, l in enumerate(lims):
                self.worker_threads.append(Thread(target=self.calc_parallel_thread, args=(self.cells, dt, l, count, )) )

        for t in self.worker_threads:
            t.start()
        for t in self.worker_threads:
            t.join()
        self.worker_threads = []
        # time.sleep(2)

        # TODO: Create threads *once* and keep them in memory. Feed them new data via the
        #       lims lists (they usually would not even change in static geometries / spaces!!)
        #       start them and do *not* join them after work has finished. Give them Events to wait for.

    def calc_parallel_thread(self, cells, dt, lims:((int,int),(int,int),(int,int)), number):
        pass
        # TODO: run force calculations in the shared object in the limits (lims)
        # TODO: If weird issues happen at the connection line, check multithreading.Lock()
        #       to lock while writing to the shared cells list / cells itself.
        # (s_x, s_y, s_z) = self.size
        for x in range(lims[0][0], lims[0][1]):
            for y in range(lims[1][0], lims[1][1]):
                for z in range(lims[2][0], lims[2][1]):
                    cell = cells[x][y][z]
                    if cell.__class__ == Cell:
                        cell.force_calculate()

    def tick_force_calculations_parallel_process(self, dt, cuts:(int,int,int)=(1,0,0), autocut:bool=True):
        """
        Parallelizes force calculations. If autocut is True (default), it will try to
        determin the number of available cores and cut the calculations into chunks if
        a gain in computing time is considered possible.

        The number of cuts in each dimension can be set manually by using the cuts parameter. It
        must be a tuple of three integers, each number indicating the number of cuts in each dimension
        of the finite elements array.

        dt is the time step length of the calculation.

        TODO: Currently cuts and autocut don't do anything!

        :param dt: int
        :param cuts: tuple of three integers
        :param autocut: boolean flag
        :return:
        """
        (s_x, s_y, s_z) = self.size

        # TODO: Create Worker Threads ONCE and keep them, waiting for an event. (Find out how: multithreading.Event)

        # TODO: dim and cuts don't do anything yet to parallelize the calculations
        with Manager() as manager:
            # create an empty, multiprocess shared array
            m_cells_force = manager.list()
            for i in range(len(self.cells)):
                j_data = manager.list()
                for j in range(len(self.cells)):
                    k_data = manager.list()
                    for k in range(len(self.cells)):
                        k_data.append([0, 0, 0])
                    j_data.append(k_data)
                m_cells_force.append(j_data)

            if len(self.worker_processes) == 0:
                lims = []
                lims.append( ((0, round(s_x / 2)), (0, s_y), (0, s_z)) )
                lims.append( ((round(s_x / 2), s_x), (0, s_y), (0, s_z)) )
                # lims.append( ((0, round(s_x / 2)), (round(s_y / 2), s_y), (0, s_z)) )
                # lims.append( ((round(s_x / 2), s_x), (round(s_y / 2), s_y), (0, s_z)) )

                # create and start threads
                for count, l in enumerate(lims):
                    #self.worker_processes.append(Thread(target=self.calc_parallel_process, args=(self.cells, dt, l, count, )) )
                    self.worker_processes.append(
                        Process(target=self.calc_parallel_process, args=(m_cells_force, dt, l, count,)) )

            for p in self.worker_processes:
                p.start()
            for p in self.worker_processes:
                p.join()
            self.worker_processes = []

            for x in range(self.size[0]):
                for y in range(self.size[1]):
                    for z in range(self.size[2]):
                        self.cells[x][y][z].set_force(m_cells_force[x][y][z])

            # time.sleep(2)

            # TODO: Create threads *once* and keep them in memory. Feed them new data via the
            #       lims lists (they usually would not even change in static geometries / spaces!!)
            #       start them and do *not* join them after work has finished. Give them Events to wait for.


    def calc_parallel_process(self, cells_f, dt, lims:((int,int),(int,int),(int,int)), number):
        # TODO: run force calculations in the shared object in the limits (lims)
        # TODO: If weird issues happen at the connection line, check multithreading.Lock()
        #       to lock while writing to the shared cells list / cells itself.
        # (s_x, s_y, s_z) = self.size
        cells = self.cells
        print("x:", len(cells))
        print("y:", len(cells[0]))
        print("z:", len(cells[0][0]))
        for x in range(lims[0][0], lims[0][1]):
            for y in range(lims[1][0], lims[1][1]):
                for z in range(lims[2][0], lims[2][1]):
                    cell = cells[x][y][z]
                    if cell.__class__ == Cell:
                        v_add_to(cells_f[x][y][z], cell.force_calculate())

    def get_displacement_matrix(self, range_x:(int, int)=(None, None), range_y:(int, int)=(None, None), range_z:(int, int)=(None, None)):
        (size_x, size_y, size_z) = self.size

        start_x = 0 if range_x[0] is None else max(range_x[0], 0)
        end_x = size_x if range_x[1] is None else min(range_x[1], size_x)
        start_y = 0 if range_y[0] is None else max(range_y[0], 0)
        end_y = size_y if range_y[1] is None else min(range_y[1], size_y)
        start_z = 0 if range_z[0] is None else max(range_z[0], 0)
        end_z = size_z if range_z[1] is None else min(range_z[1], size_z)

        matrix = []
        sum = 0
        for y in range(start_y, end_y):
            x_array = []
            for x in range(start_x, end_x):
                z_array = []
                for z in range(start_z, end_z):
                    #line += str(round(self.cells[x][y][z].position)) + " "
                    # line += str(round(self.cells[x][y][z].get_position()[2])) + " "
                    pos = self.cells[x][y][z].get_position()[2]
                    vel = self.cells[x][y][z].velocity[2]
                    z_array.append(pos)
                    sum += vel   # formerly pos
                x_array.append(z_array)
            matrix.append(x_array)

        return matrix, sum

    def __str__(self):
        text = ""
        for c in self.cells:
            if c is not None:
                text += str(c) + "\n"
        return text



class Viz:
    def __init__(self):
        pass

    def display_matrix(self, matrix):
        pydpyl.clear()
        text = ""
        for x in range(len(matrix)):
            line = ""
            for y in range(len(matrix[0])):
                # z=0 - ignoring other z levels!
                line += str(round(matrix[x][y][0]) ) + " "
            text += line + "\n"
        print(text)


class VizPG:
    def __init__(self):
        import pygame
        self.pg = pygame
        self.pg.init()


class VizPLT:
    def __init__(self):
        # import pygame as pg
        import matplotlib.pyplot
        import numpy
        self.plt = matplotlib.pyplot
        self.np = numpy

        self.fig, self.ax_lst = self.plt.subplots(1,1)
        # self.ax_lst = self.ax_lst.ravel()

        self.data = self.np.random.rand(3, 3)
        # self.im = self.ax_lst[0].imshow(self.data)
        self.im = self.ax_lst.imshow(self.data)
        self.fig.canvas.draw()
        self.fig.show()

    def display_matrix(self, matrix):
        self.data = self.np.array(matrix)
        self.im = self.ax_lst.imshow(self.data, vmin=-0.5, vmax=.5)
        self.ax_lst.draw_artist(self.ax_lst.patch)
        self.ax_lst.draw_artist(self.im)
        self.fig.canvas.blit(self.ax_lst.bbox)
        self.fig.canvas.flush_events()

        #self.fig.gca().relim()
        #self.fig.gca().autoscale_view()
        #self.plt.pause(0.00000000001)


    def close(self):
        # empty data chunks
        self.write_to_file()
        # get scaling factor from temp files and convert to wave file
        factor_output = self.analyze_temp_file(self.fullpath_output_tmp)
        self.convert_temp_file(self.fullpath_output_tmp, self.fullpath_output, factor_output)
        # TODO: delete temp file

        if self.capture_input:
            factor_input = self.analyze_temp_file(self.fullpath_input_tmp)
            self.convert_temp_file(self.fullpath_input_tmp, self.fullpath_input, factor_input)
            # TODO: delete temp file


class App:
    def __init__(self):
        self.viz = VizPLT()
        self.getch = pydpyl.Getch(0.0)
        self.body = None
        self.timy = Timy()
        self.output = None

        self.config = configparser.ConfigParser()
        self.make_config()

    def make_config(self):
        # TODO: load config via simple menu
        if not self.select_config():
            self.config["DEFAULT"] = {}
            self.config["DEFAULT"]["signaltype"] = "sine"
            self.config["sine"] = {}
            self.config["sine"]["freq"] = 440   # Hz
            self.config["sine"]["duration"] = 1   # seconds

    def select_config(self):
        # TODO: List config files, enumerate, menu, select

        # for now: Hardcoded file_out
        self.config.read("./config/sine_440hz.conf")
        print(self.config.sections())
        for key in self.config["DEFAULT"]:
            #print(self.config["DEFAULT"]["input"]); time.sleep(3)
            print(key)
        # TODO: check/parse config

        # set up wave file_out output
        if self.config["DEFAULT"]["output"] == "file":
            fconf = self.config["output_file"]
            # set up two capture streams
            #self.output = WaveOutpu()

            #self.output.add_stream("rx", fconf["filename"]+"_rx", int(fconf["smplrate"]), int(fconf["smplwidth"]), fconf["channels"]
            #self.output.add_stream("tx", fconf["filename"]+"_tx", int(fconf["smplrate"]), int(fconf["smplwidth"]), fconf["channels"])
            self.output = WaveOutput(fconf["filename"], int(fconf["smplrate"]), int(fconf["smplwidth"]), fconf["channels"], bool(fconf["capture_input"]))

        return True    # if config has been selected / loaded

    def run(self):
        global MULTITHREAD, MULTIPROCESS

        # -------- SETTING UP THE PROGRAM RUN
        #  SIGNALS SETUP
        #  proper closing
        signal.signal(signal.SIGINT, self.sigint_handler)
        signal.signal(signal.SIGQUIT, self.sigint_handler)
        signal.signal(signal.SIGTERM, self.sigint_handler)
        # ensure at least file closing
        # signal.signal(signal.SIGSEGV, self.end)  # does not work
        signal.signal(signal.SIGILL, self.end)

        # -------- SETTING UP THE SIMULATION ENVIRONMENT
        # create a slab type body
        c_d = self.config["DEFAULT"]
        size_x = int(c_d["size_x"])
        size_y = int(c_d["size_y"])
        size_z = int(c_d["size_z"])
        element_length = float(c_d["element_length"])
        hardness = (0,0,5000000)
        self.body = Body(element_length, slab={"size":(size_x,size_y,size_z), "hardness":hardness, "base":(21,16,0)})
        #self.body = Body(slab=(60, 30, 1), base=(30, 18, 0))

        self.body.cells[0][0][0].set_debug(True)

        # SIGNAL INPUT
        # set up sine signal generator
        sine = SG.SGsine()
        sine.setup(amplitude=self.config["sine"]["amplitude"], freq=self.config["sine"]["freq"])
        if self.config["DEFAULT"]["input"] == "sine":
            # f = float(self.config["sine"]["freq"])
            dt = float(self.config["sine"]["dt"])
            # w = 2 * math.pi * f
            duration = float(self.config["sine"]["duration"])
            #z_0 = float(self.config["sine"]["amplitude"])
            # print(f"{f} {dt} {w} {duration} {z_0}")

        # SIGNAL OUTPUT
        # create the output handler (i.e. wave file generator)
        self.output.open()

        # data chunks for histograms
        signal_sent = []
        signal_received = []

        # setup for loop, data and vis refresh
        t = 0   # time of start
        step = 0    # counter of calculation steps
        log_calc_step = 1000  # show data every x steps
        log_vis_step = int(self.config["DEFAULT"]["refresh"])  # visualize every x steps

        # location of excitation (here: golden ratio)
        ex_x = round(21)
        ex_y = round(16)

        while not QUIT:
            text_output = ""
            # a very basic excitation (sine curve for t=0 to 100
            if t < duration or duration < 0:
                # displacement = (0, 0, z_0 * math.sin(w * t) * 5)
                factor_z = 700000

                excitation = (0, 0, factor_z * sine(t))  # + (z_0/ 2) * math.sin(w*2.3 * t) )
                for dx in (-1,0,1,2):
                    for dy in (-1,0,1,2):
                        # 2nd version: Excite via locally induced force (BETTER!!!)
                        self.body.cells[ex_x+dx][ex_y+dy][0].force_receive(excitation)
            else:
                excitation = (0,0,0)

            if step % log_vis_step == 0:
                time_calc = self.timy.stop("Calc") / log_vis_step
                # print(self.timer('self.viz.display_matrix(self.body.get_displacement_matrix(range_x=(None,None), range_y=(None,None), range_z=(0,1)))').timeit())
                # print(self.timer("print('hi')").timeit(number=1))

                # VISUALIZE the data and take the time
                self.timy.start("Viz")
                # TODO: 3D visualization
                # get the vizualizationable data as an array (matrix) for 2D painting (for now)
                matrix, displacement_sum = self.body.get_displacement_matrix(range_x=(None, None), range_y=(None, None),
                                                                             range_z=(0, 1))
                self.viz.display_matrix(matrix)
                print(" multithread: ", MULTITHREAD, "multiprocess:", MULTIPROCESS, "t_viz :", self.timy.stop("Viz"),
                      "t_calc:", time_calc)
                self.timy.start("Calc")

                # save history of signal sent and received
                if len(signal_sent) > 100:
                    signal_sent = signal_sent[2:]
                    signal_received = signal_received[2:]
                signal_sent.append(excitation[2])
                signal_received.append(displacement_sum)
                print(f"Signals: tx: {excitation} rx: {displacement_sum} lenghts: {len(signal_sent)}, {len(signal_received)}")
                pydpyl.printhist(signal_sent, height=5, columnw=1)
                pydpyl.printhist(signal_received, height=5, columnw=1)
                print(f"step {step}: t={t} dt={dt}")

            # run the simulation one time step
            sum_pos, sum_vel = self.body.tick(dt)

            # write the singals to the output handler
            self.output.write(sum_vel, excitation[2])

            # A bit of logging to the terminal (deprecated)
            if step % log_calc_step == 0:
                pass
                # TODO: Nothing happens here. Remove?

            # next_step / time-step
            step += 1
            t += dt

            # EVENT HANDLING (KEYBOARD)
            # TODO: CAREFUL! THIS WILL MESS UP CAPTURE FILES (wave files)
            entry = self.getch()
            if entry in ("x", "X", "q", "Q"):
                break
            elif entry == "+":
                dt += 0.1
            elif entry == "-":
                dt = max(0.1, dt - 0.1)
        self.end()

    def end(self, statement=None):
        if statement is not None:
            print(f"quitting solidwavsim: {statement}")
        self.output.close()
        quit()

    def sigint_handler(self, signum, frame):
        global QUIT
        print(f"SIGINT received ({signum}, {frame})")
        QUIT = True

if __name__ == "__main__":
    app = App()
    app.run()