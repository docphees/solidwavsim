#!python3
import math

VERSION, SUBVERION = "0", "01"

class SGsine:
    def __init__(self):
        self.w:float = None
        self.amplitude:float = None
        self.phase:float = None
        self.offset:float = None
        self.wavenumber:float = None
        self.position:float = None

    def setup(self, amplitude, freq=None, period=None, omega=None, angle_velocity=None, phase=0, offset=0, wavenumber=0, wavelength=None, position=0):
        """
        Setting up your sine generator.
        Required:
        - Amplitude
        - one of these: frequency, period, omega or angle velocity (will be converted to omega)
        Optional:
        - phase
        - offset of oscillation
        - position in direction of travel in combination with the following:
            - one of wavenumber or wavelength (will be converted to wavenumber)

        Usage: oscillation(t) = amplitude * sin(wavenumber * position + omega * t + phase) + offset

        :param amplitude: float
        :param freq: float
        :param period: float
        :param omega: float
        :param angle_velocity: float (in degrees per second)
        :param phase: float
        :param offset: float
        :param wavenumber: float
        :param wavelength: float
        :param position: float
        :return:
        """
        self.amplitude = float(amplitude)
        self.w = 0
        if freq is not None:
            self.w = 2 * math.pi * float(freq)
        elif period is not None:
            self.w = 2 * math.pi / float(period)
        elif omega is not None:
            self.w = float(omega)
        elif angle_velocity is not None:
            self.w = float(angle_velocity)/360 * 2 * math.pi

        self.phase = float(phase)

        self.offset = float(offset)

        # spacial offset (in direction of travel)
        self.wavenumber = float(wavenumber)
        if wavelength is not None:
            self.wavenumber = 2 * math.pi / float(wavelength)
        self.position = float(position)

    def set_position(self, position):
        self.position = float(position)

    def __call__(self, t, position=0):
        self.set_position(position)
        value = self.amplitude * math.sin(self.wavenumber * self.position + self.w * t + self.phase) + self.offset
        return value

if __name__ == "__main__":
    print("signalgenerator.py")
    print("==================",f"v{VERSION}.{SUBVERSION}")
    print("Provides various signal generators as classes:")
    print("")
    print("- SGsine")
    print("A simple sine curve generator")
    print("")
    print("- SGramp")
    print("A simple frequency ramp")
