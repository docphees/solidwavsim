
# STRUCTURE
## TODO: Geometrie generator files / geometrie files
    - allows the generation of solid bodies / bodies of air etc.
## TODO: Signal files
    - One file can contain several signals or just one? 
## TODO: Experiment files
    - The file contains the full setup of an experiment:
    - Solids, Air, signals, excision points come together here and are "linked"
    - This might simply be a python file with a collection of functions/definitions
    - Data collection / Visualization may be defined here
## TODO: Experiment_meta file
    - This reads the Experiment files definitions and puts everything together (end boss level)

# CALCULATION
## TODO: Numpy would help to calculate. But how?
    - possibly the "neighbours" could be emulated by a matrix x matrix multiplication, or even matrix x vector?
    - the results would have to be corner-cut, but numpy would be the perfect tool for this
## TODO: Multithreading
    Create worker threads which get chunks of the cells list from body to calculate:
    - Borders should be overlapping, so the forces can be stitched together.
    - Initially cut in half, later try quartering etc.
    - Possibly set a "chunks" variable, or even detecting available cores/threads for optimal performance
## TODO: try 3D visualization: give cells a material id, which can be handled differently by
    the visualizer
    - air transparent, only high pressure gets painted, rest clear
    - solids semi transparent, different color-scale etc.
    - each color scale should auto-calibrate to high/low parts, only regarding their associated material
    - color scales should react to wider spread immediately, pausing for a while and only decay over several cycles
